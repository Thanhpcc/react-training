const initState = [];

export default (state = initState, { type, payload }) => {
    console.log('datahehe: ',payload);
    switch (type) {
        case 'users/ADD_USER':
            return payload
  
        case 'users/EDIT_USER': {
            return [
                ...state.map((u, i) => {
                    if( i === payload.userId){
                        return payload.user;
                    }
                    return u;
                })
            ]
        }
        case 'users/DEL_USER': {
            return [
                ...state.slice(0,payload),...state.slice(payload + 1)
            ] 
        }
        case 'users/': {
            return state.concat(payload)
        }
        default:
            return state;
    }
}