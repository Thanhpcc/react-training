import { API } from './common';

export const editUser = (userId, user) => 
    API.put('/users/EDIT_USER', { userId, user });

export const delUser = (userId) => 
    API.delete('/users/EDIT_USER', userId);

export const addUser = (user) =>
    API.post('/users/ADD_USER', user);

export const getUser = () => 
    API.get('/users');
