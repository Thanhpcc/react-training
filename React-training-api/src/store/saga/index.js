import { fork, all } from 'redux-saga/effects';
import listUser from './listUser';

const rootSaga = function* () {
    yield all([
        ...listUser.map(watcher => fork(watcher))
    ])
};

export default rootSaga;
