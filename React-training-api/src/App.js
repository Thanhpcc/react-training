import React from 'react';
import ListUser from './components/ListUser';
import Form from './components/Form';

function App() {
  return (
    <div className="App">
      <Form />
      <ListUser />
    </div>
  );
}

export default App;
