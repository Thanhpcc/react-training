import React from 'react';
import { connect } from "react-redux";
import UserItem from './UserItem';
import { getUser, editUser, delUser } from '../store/action/listUser'


class ListUser extends React.Component{

    componentDidMount(){
        console.log('this.props: ', this.props);
        this.props.getUser()
    }


    render(){
        console.log('hehe111', this.props)
        return (
            <table className="table table-hover">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Birthday</th>
                        <th>Gender</th>
                    </tr>
                </thead>
                <tbody>
                    {this.props.listUser.map((item, index)=>(
                        <UserItem item={item} key={index} index={index} editUser={this.props.editUser} delUser={this.props.delUser}/>
                    ))}
                </tbody>
            </table>    
        )
    }
}

const mapStateToProps = (state)=> state;

export default connect(mapStateToProps, { editUser, getUser, delUser })(ListUser)
