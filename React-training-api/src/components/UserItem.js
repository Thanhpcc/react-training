import React from 'react';

export default class UserItem extends React.Component {

    render() {
        const { item } = this.props;
        const { index } = this.props;
        return (
            <tr>
                <td>
                    {item.txtName}
                </td>
                <td>
                    {item.txtBirthdate}
                </td>
                <td>
                    {item.txtGender}
                </td>
                <td>
                    <button
                        type="button"
                        className="btn btn-default"
                        onClick={() => this.props.editUser(index, {
                            txtName: 'hihi',
                            txtBirthdate: '789654',
                            txtGender: 'male'
                        })}
                    >
                        Edit
                    </button>
                </td>
                <td>
                    <button type="button" className="btn btn-default" onClick={() => this.props.delUser(index)}>
                        Delete
                    </button>
                </td>
            </tr>
        )
    }
}